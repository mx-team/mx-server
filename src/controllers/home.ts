import { Request, Response } from "express";
import path from "path";

/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
//   res.render("home", {
//     title: "Home"
//   });
	res.sendFile('index.html', { root: path.join(__dirname, '../public') });
};
